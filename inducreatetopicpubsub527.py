from google.cloud import pubsub_v1

def handler(request):

 
    project_id = "sigma-qa-testing"
    topic_id = "indunewpubsub01"

    publisher = pubsub_v1.PublisherClient()
    topic_path = publisher.topic_path(project_id, topic_id)

    topic = publisher.create_topic(request={"name": topic_path})

    print(f"Created topic: {topic.name}") 

    return "Successfully executed"
